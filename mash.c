#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
//test
int mash_cd(char **args);
int mash_help(char **args);
int mash_exit(char **args);
char *builtin_str[] = {"cd", "help", "exit"};
int (*builtin_func[])(char **) = {&mash_cd, &mash_help, &mash_exit};
int mash_num_builtins() { return sizeof(builtin_str) / sizeof(char *); }
int mash_cd(char **args) {
  if (args[1] == NULL) {
    fprintf(stderr, "mash: expected argument to \"cd\"\n");
  } else {
    if (chdir(args[1]) != 0) {
      perror("mash");
    }
  }
  return 1;
}

int mash_help(char **args) {
  int i;
  printf("MASH version 0.0.0.0.1\n");
  printf("Type program names and arguments, and hit enter.\n");
  printf("The following are built in:\n");

  for (i = 0; i < mash_num_builtins(); i++) {
    printf("  %s\n", builtin_str[i]);
  }

  printf("Use the man command for information on other programs.\n");
  return 1;
}
int mash_exit(char **args) { return 0; }
int mash_launch(char **args) {
  pid_t pid;
  int status;

  pid = fork();
  if (pid == 0) {
    // Child process
    if (execvp(args[0], args) == -1) {
      perror("mash");
    }
    exit(EXIT_FAILURE);
  } else if (pid < 0) {
    // Error forking
    perror("mash");
  } else {
    // Parent process
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  return 1;
}
int mash_execute(char **args) {
  int i;

  if (args[0] == NULL) {
    return 1;
  }

  for (i = 0; i < mash_num_builtins(); i++) {
    if (strcmp(args[0], builtin_str[i]) == 0) {
      return (*builtin_func[i])(args);
    }
  }

  return mash_launch(args);
}
char *mash_read_line(void) {
#ifdef MASH_USE_STD_GETLINE
  char *line = NULL;
  ssize_t bufsize = 0; // have getline allocate a buffer for us
  if (getline(&line, &bufsize, stdin) == -1) {
    if (feof(stdin)) {
      exit(EXIT_SUCCESS); // We received an EOF
    } else {
      perror("mash: getline\n");
      exit(EXIT_FAILURE);
    }
  }
  return line;
#else
#define MASH_RL_BUFSIZE 1024
  int bufsize = MASH_RL_BUFSIZE;
  int position = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "mash: allocation error\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    // Read a character
    c = getchar();

    if (c == EOF) {
      exit(EXIT_SUCCESS);
    } else if (c == '\n') {
      buffer[position] = '\0';
      return buffer;
    } else {
      buffer[position] = c;
    }
    position++;

    // If we have exceeded the buffer, reallocate.
    if (position >= bufsize) {
      bufsize += MASH_RL_BUFSIZE;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "mash: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
  }
#endif
}

#define MASH_TOK_BUFSIZE 64
#define MASH_TOK_DELIM " \t\r\n\a"
char **mash_split_line(char *line) {
  int bufsize = MASH_TOK_BUFSIZE, position = 0;
  char **tokens = malloc(bufsize * sizeof(char *));
  char *token, **tokens_backup;

  if (!tokens) {
    fprintf(stderr, "mash: allocation error\n");
    exit(EXIT_FAILURE);
  }

  token = strtok(line, MASH_TOK_DELIM);
  while (token != NULL) {
    tokens[position] = token;
    position++;

    if (position >= bufsize) {
      bufsize += MASH_TOK_BUFSIZE;
      tokens_backup = tokens;
      tokens = realloc(tokens, bufsize * sizeof(char *));
      if (!tokens) {
        free(tokens_backup);
        fprintf(stderr, "mash: allocation error\n");
        exit(EXIT_FAILURE);
      }
    }

    token = strtok(NULL, MASH_TOK_DELIM);
  }
  tokens[position] = NULL;
  return tokens;
}
void mash_loop(void) {
  char *line;
  char **args;
  int status;

  do {
    printf("> ");
    line = mash_read_line();
    args = mash_split_line(line);
    status = mash_execute(args);

    free(line);
    free(args);
  } while (status);
}
int main(int argc, char **argv) {
  mash_loop();
  return EXIT_SUCCESS;
}
